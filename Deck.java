import java.util.Random;
public class Deck{
	private Card[] cards;
	private int numberOfCards;
	private Random rng;
	
	public Deck(){
		this.rng = new Random();
		this.cards = new Card[52];
		int i = 0;
		int[] ranks = new int[]{1,2,3,4,5,6,7,8,9,10,11,12,13};
		String[] suits = new String[]{"Hearts", "Clubs", "Spades", "Diamond"};
		for (int r: ranks){
			for(String s : suits){
				cards[i] = new Card(s, r);
				i++;
			}
		}
		this.numberOfCards = i;
	}
	
	public String toString(){
		String s = "Your deck is "+numberOfCards+ " cards long and here are the cards: \n";
		for(int i=0; i<numberOfCards; i++){
			s+= cards[i].toString()+"\n";
		}
		return s;
	}
	
	public int length(){
		return this.numberOfCards;
	}
	
	public Card drawTopCard(){
		Card lastCard = null;
		if(numberOfCards>=0){
			lastCard = cards[numberOfCards-1];
			numberOfCards--;
		}
        return lastCard;
	}
	
	public void shuffle(){
		for(int i = 0; i<numberOfCards; i++){
			int randomCard = rng.nextInt(numberOfCards);
			Card temp = cards[i];
            cards[i] = cards[randomCard];
            cards[randomCard] = temp;
			}
		}
	
}