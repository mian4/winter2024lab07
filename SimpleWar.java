public class SimpleWar{
	public static void main(String[] args){
		Deck theDeck = new Deck();
		theDeck.shuffle();
		
		//System.out.println(theDeck);
		
		int playerOnePoints = 0;
		int playerTwoPoints = 0;
		//System.out.println("First and Second card :"+theDeck.drawTopCard()+", "+theDeck.drawTopCard());
		/*Card test1 = theDeck.drawTopCard();
		Card test2 = theDeck.drawTopCard();
		
		System.out.println("Player 1 Points : "+playerOnePoints);
		System.out.println("Player 2 Points : "+playerTwoPoints);
		
		System.out.println("Score of first card: "+test1+" is "+test1.calculateScore());
		System.out.println("Score of Second card: "+test2+" is "+test2.calculateScore());
	
		
		if (Math.max(test1.calculateScore(), test2.calculateScore()) == test1.calculateScore()){
			System.out.println("Winner is "+test1+" for player 1");
			playerOnePoints++;
		}else{
			System.out.println("Winner is "+test2+" for player 2");
			playerTwoPoints++;
		}
		System.out.println("Player 1 Points : "+playerOnePoints);
		System.out.println("Player 2 Points : "+playerTwoPoints);
		*/
		
		int round = 0;
		while(theDeck.length() > 0){
			Card playerOneCard = theDeck.drawTopCard();
			Card playerTwoCard = theDeck.drawTopCard();
		
			System.out.println("Player 1 Points at round "+round+": "+playerOnePoints);
			System.out.println("Player 2 Points at round "+round+": "+playerTwoPoints+"\n---------------------------------------------");
		
			System.out.println("Score of first card: "+playerOneCard+" is "+playerOneCard.calculateScore());
			System.out.println("Score of Second card: "+playerTwoCard+" is "+playerTwoCard.calculateScore());
	
		
			if (Math.max(playerOneCard.calculateScore(), playerTwoCard.calculateScore()) == playerOneCard.calculateScore()){
				System.out.println("Winner is "+playerOneCard+" for player 1");
				playerOnePoints++;
			}else{
				System.out.println("Winner is "+playerTwoCard+" for player 2");
				playerTwoPoints++;
			}
			round++;
			
		}
		System.out.println("Player 1 Final Points : "+playerOnePoints);
		System.out.println("Player 2 Final Points : "+playerTwoPoints+"\n---------------------------------------------");
		
		if(playerOnePoints>playerTwoPoints){
			System.out.println("Player 1 Wins!");
		}else if(playerOnePoints<playerTwoPoints){
			System.out.println("Player 2 Wins!");
		}else{
			System.out.println("You both won! Congrats!");
		}
		
	}

}