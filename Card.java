public class Card{
	private String suit;
	private int rank;
	
	public Card(String suit, int rank){
		this.suit = suit;
		this.rank = rank;
	}
	
	public String getSuit(){
		return this.suit;
	}
	
	public int getRank(){
		return this.rank;
	}
	
	public String toString(){
		String printRank = Integer.toString(rank);
		if(rank == 1){
			printRank = "Ace";
		}else if(rank == 11){
			printRank = "Jack";
		}else if(rank==12){
			printRank = "Queen";
		}else if(rank==13){
			printRank = "King";
		}
		return printRank +" of "+suit;
	}
	public static void main(String[] args){
		Card card1 = new Card("Hearts", 1);
		System.out.println(card1);
	}
	
	public double calculateScore(){
		double score = rank;
		if(suit.equals("Hearts"))
			score += 0.4;
		else if(suit.equals("Spades"))
			score += 0.3;
		else if(suit.equals("Diamond"))
			score += 0.2;
		else
			score += 0.1;
		
		return score;
	}
}